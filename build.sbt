name := """snap-chat-clone"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  filters,
  jdbc,
  cache,
  ws,
  evolutions,
  specs2 % Test,
  "mysql" % "mysql-connector-java" % "5.1.39",
  "com.h2database"   %  "h2"                           % "1.4.192",
  "org.scalikejdbc"  %% "scalikejdbc"                  % "2.4.2",
  "org.scalikejdbc"  %% "scalikejdbc-config"           % "2.4.2",
  "org.scalikejdbc"  %% "scalikejdbc-play-initializer" % "2.5.1",
  "org.twitter4j"    % "twitter4j-core"                % "4.0.6",
  "com.adrianhurt"   %% "play-bootstrap"               % "1.1-P24-B3",
  "jp.t2v"           %% "play2-auth"                   % "0.14.2",
  "jp.t2v"           %% "play2-auth-social"            % "0.14.2", // ソーシャルログイン
  "jp.t2v"           %% "play2-auth-test"              % "0.14.2" % "test",
  "net.kaliber"      %% "play-s3"  % "7.0.2",
  "com.google.guava" % "guava-io" % "r03",
  "io.swagger"       %% "swagger-play2" % "1.5.1",
  play.sbt.Play.autoImport.cache
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"
resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"
resolvers += "Kaliber Internal Repository" at "https://jars.kaliber.io/artifactory/libs-release-local"

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

scalikejdbcSettings

