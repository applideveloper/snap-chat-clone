import javax.inject.Inject

import controllers.routes
import play.api.http.HttpErrorHandler
import play.api.mvc.{RequestHeader, Result}
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.mvc.Results._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class ErrorHandler @Inject()(
  val messagesApi: MessagesApi
) extends HttpErrorHandler
  with I18nSupport {
  override def onClientError(request: RequestHeader, statusCode: Int, message: String): Future[Result] = {
    statusCode match {
      case play.api.http.Status.REQUEST_ENTITY_TOO_LARGE => {
        Future.successful(Redirect(routes.Application.index())).map(_.flashing("failure" -> Messages("FileSizeTooLarge")))
      }
      case _ => {
        Future.successful(
          Status(statusCode)("A client error occurred: " + message)
        )
      }
    }
  }

  override def onServerError(request: RequestHeader, exception: Throwable): Future[Result] = {
    Future.successful(
      InternalServerError("A server error occurred: " + exception.getMessage)
    )
  }
}
