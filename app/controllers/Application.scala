package controllers

import javax.inject.Inject

import models._
import play.api.mvc.Results._
import play.api.mvc._
import scalikejdbc.DB

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}
import scala.reflect.{ClassTag, classTag}
import jp.t2v.lab.play2.auth._
import jp.t2v.lab.play2.auth.social.providers.twitter.{TwitterController, TwitterProviderUserSupport}
import play.api.Environment
import play.api.cache.CacheApi
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, Messages, MessagesApi}

class Application @Inject() (
  val environment: Environment,
  val cacheApi: CacheApi,
  val messagesApi: MessagesApi
) extends Controller
  with OptionalAuthElement
  with AuthConfigImpl
  with Logout
  with I18nSupport {

  private val fileForm = Form {
    "file" -> nonEmptyText
  }

  def index = StackAction { implicit request =>
    DB.readOnly { implicit session =>
      val userOpt = loggedIn
      val twitterUserOpt = userOpt.flatMap(u => TwitterUser.findByUserId(u.id))

      userOpt match {
        case Some(user) => {
          twitterUserOpt match {
            case Some(twitterUser) => {
              Ok(views.html.index(user, fileForm))
            }
            case None => Ok(views.html.link())
          }
        }
        case None => Ok(views.html.logout())
      }
    }
  }

  def logout = Action.async { implicit request =>
    gotoLogoutSucceeded.map(_.flashing("success" -> Messages("Logout")))
  }
}

trait AuthConfigImpl extends AuthConfig {
  type Id = Long
  type User = models.User
  type Authority = models.Authority
  val idTag: ClassTag[Id] = classTag[Id]
  val sessionTimeoutInSeconds: Int = 3600

  val cacheApi: CacheApi

  override lazy val idContainer: AsyncIdContainer[Id] = AsyncIdContainer(new CacheIdContainer[Id]())

  def resolveUser(id: Id)(implicit ctx: ExecutionContext): Future[Option[User]] =
    Future.successful(DB.readOnly { implicit session =>
      User.find(id)
    })

  def loginSucceeded(request: RequestHeader)(implicit ctx: ExecutionContext): Future[Result] =
    Future.successful(Redirect(routes.Application.index()))

  def logoutSucceeded(request: RequestHeader)(implicit ctx: ExecutionContext): Future[Result] =
    Future.successful(Redirect(routes.Application.index))

  def authenticationFailed(request: RequestHeader)(implicit ctx: ExecutionContext): Future[Result] =
    Future.successful(Redirect(routes.Application.index))

  override def authorizationFailed(request: RequestHeader, user: User, authority: Option[Authority])(implicit ctx: ExecutionContext) =
    Future.successful(Forbidden("no permission"))

  def authorize(user: User, authority: Authority)(implicit ctx: ExecutionContext): Future[Boolean] = Future.successful {
    true
  }

}

class TwitterAuthController @Inject() (
  val environment: Environment,
  val cacheApi: CacheApi,
  val messagesApi: MessagesApi
) extends TwitterController
  with AuthConfigImpl
  with TwitterProviderUserSupport
  with I18nSupport {

  override def onOAuthLinkSucceeded(token: AccessToken, consumerUser: User)(implicit request: RequestHeader, ctx: ExecutionContext): Future[Result] = {
    retrieveProviderUser(token).map { providerUser =>
      DB.localTx { implicit session =>
        TwitterUser.save(consumerUser.id, providerUser)
        Redirect(routes.Application.index)
      }
    }
  }

  override def onOAuthLoginSucceeded(token: AccessToken)(implicit request: RequestHeader, ctx: ExecutionContext): Future[Result] = {
    retrieveProviderUser(token).flatMap { providerUser =>
      DB.localTx { implicit session =>
        TwitterUser.findById(providerUser.id) match {
          case None =>
            val id = User.create(providerUser.screenName, providerUser.profileImageUrl).id
            TwitterUser.save(id, providerUser)
            gotoLoginSucceeded(id).map(_.flashing("success" -> Messages("Login")))
          case Some(tu) =>
            gotoLoginSucceeded(tu.userId).map(_.flashing("success" -> Messages("Login")))
        }
      }
    }
  }
}