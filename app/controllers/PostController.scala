package controllers

import java.nio.file.FileAlreadyExistsException
import javax.inject.Inject

import jp.t2v.lab.play2.auth.OptionalAuthElement
import play.api.cache.CacheApi
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.mvc.{Action, AnyContent, Controller}

class PostController @Inject()(
  val cacheApi: CacheApi,
  val messagesApi: MessagesApi
) extends Controller
  with OptionalAuthElement
  with AuthConfigImpl
  with I18nSupport {

  private val fileForm = Form {
    "file" -> nonEmptyText
  }

  def upload = Action(parse.multipartFormData) { request =>
    play.Logger.debug(s"Called upload function" + request)
    request.body.file("file").map { file =>
      import java.io.File
      val filename = file.filename
      val contentType = file.contentType
      play.Logger.debug(s"File name $filename, content type: $contentType")
      try {
        file.ref.moveTo(new File("./public/videos/", filename), true)
      } catch {
        case fileAlreadyExistsException: FileAlreadyExistsException => {
          play.Logger.debug(fileAlreadyExistsException.getLocalizedMessage)
          Redirect(routes.Application.index()).flashing("failure" -> Messages("FileAlreadyExists"))
        }
      }
      Redirect(routes.Application.index()).flashing("success" -> Messages("UploadSuccess"))
    }.getOrElse {
      Redirect(routes.Application.index()).flashing("failure" -> Messages("FileIsNothing"))
    }
  }
}
