package controllers

import javax.inject.Inject

import jp.t2v.lab.play2.auth.OptionalAuthElement
import models.TwitterUser
import play.api.cache.CacheApi
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Controller}
import scalikejdbc.DB

class UsersController @Inject()(
  val cacheApi: CacheApi,
  val messagesApi: MessagesApi
) extends Controller
  with OptionalAuthElement
  with AuthConfigImpl
  with I18nSupport {
  def index = StackAction { implicit request =>
    DB.readOnly { implicit session =>
      val userOpt = loggedIn
      val twitterUserList = TwitterUser.findAll
      userOpt match {
        case Some(user) => Ok(views.html.users.index(user, twitterUserList))
        case None => Ok(views.html.logout())
      }
    }
  }

  def show(userId: Long) = StackAction { implicit request =>
    DB.readOnly { implicit session =>
      val userOpt = loggedIn
      val twitterUserOpt = userOpt.flatMap(u => TwitterUser.findByUserId(u.id))

      (userOpt, twitterUserOpt) match {
        case (Some(user), Some(twitterUser)) =>  Ok(views.html.users.show(user, twitterUser))
        case _ => Ok(views.html.logout())
      }
    }
  }
}
