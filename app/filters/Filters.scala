package filters

import javax.inject.Inject

import play.api.http.HttpFilters
import play.filters.cors.CORSFilter
import play.filters.csrf.CSRFFilter
import play.filters.headers.SecurityHeadersFilter


class Filters @Inject()(
  csrfFilter: CSRFFilter,
  securityHeadersFilter: SecurityHeadersFilter,
  corfFilter: CORSFilter
) extends HttpFilters {
  def filters = Seq(csrfFilter, securityHeadersFilter, corfFilter)
}
