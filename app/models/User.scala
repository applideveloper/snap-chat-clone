package models

import jp.t2v.lab.play2.auth.social.providers
import scalikejdbc._
import jp.t2v.lab.play2.auth.social.providers.{facebook, twitter}

sealed trait Authority
case object Admin extends Authority
case object Normal extends Authority

case class User(id: Long, name: String, avatarUrl: String)

case class TwitterUser(
  userId: Long,
  id: Long,
  screenName: String,
  profileImageUrl: String,
  accessToken: String,
  accessTokenSecret: String)

object User {

  def *(rs: WrappedResultSet) = User(
    rs.long("id"),
    rs.string("name"),
    rs.string("avatar_url")
  )

  def create(name: String, avatarUrl: String)(implicit session: DBSession): User = {
    DB.localTx { implicit session =>
      val id = sql"INSERT INTO users(name, avatar_url) VALUES ($name, $avatarUrl)".updateAndReturnGeneratedKey.apply()
      User(id, name, avatarUrl)
    }
  }

  def find(id: Long)(implicit session: DBSession): Option[User] = {
    DB.readOnly { implicit session =>
      sql"SELECT * FROM users WHERE id = $id".map(*).single().apply()
    }
  }

}

object TwitterUser {

  def *(rs: WrappedResultSet) = TwitterUser(
    rs.long("user_id"),
    rs.long("id"),
    rs.string("screen_name"),
    rs.string("profile_image_url"),
    rs.string("access_token"),
    rs.string("access_token_secret")
  )

  def findAll(implicit session: DBSession): List[TwitterUser] = {
    sql"SELECT * FROM twitter_users".map(*).list().apply()
  }

  def findById(id: Long)(implicit session: DBSession): Option[TwitterUser] = {
    sql"SELECT * FROM twitter_users WHERE id = $id".map(*).single().apply()
  }

  def findByUserId(userId: Long)(implicit session: DBSession): Option[TwitterUser] = {
    sql"SELECT * FROM twitter_users WHERE user_id = $userId".map(*).single().apply()
  }

  def save(userId: Long, twitterUser: twitter.TwitterUser)(implicit session: DBSession): TwitterUser = {
    val id = twitterUser.id
    val screenName = twitterUser.screenName
    val profileImageUrl = twitterUser.profileImageUrl
    val accessToken = twitterUser.accessToken
    val accessTokenSecret = twitterUser.accessTokenSecret
    sql"""INSERT INTO twitter_users(user_id, id, screen_name, profile_image_url, access_token, access_token_secret)
          VALUES ($userId, $id, $screenName, $profileImageUrl, $accessToken, $accessTokenSecret)""".update.apply()
    TwitterUser(userId, id, screenName, profileImageUrl, accessToken, accessTokenSecret)
  }

}

