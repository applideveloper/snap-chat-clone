# user table schema

# --- !Ups

CREATE TABLE users (
  id serial PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  avatar_url VARCHAR(1000) NOT NULL
) engine=innodb charset=utf8mb4;

CREATE TABLE twitter_users (
  user_id INTEGER NOT NULL UNIQUE REFERENCES users(id),
  id bigint PRIMARY KEY,
  screen_name VARCHAR(100) NOT NULL,
  profile_image_url VARCHAR(1000) NOT NULL,
  access_token VARCHAR(1000) NOT NULL,
  access_token_secret VARCHAR(1000) NOT NULL
) engine=innodb charset=utf8mb4;

# --- !Downs
 
DROP TABLE users;
DROP TABLE twitter_users;